import codegeneration.CodeGenerator;
import codegeneration.VariableGenerator;
import semantic.LabelTable;
import tree.statement.StatementNode;

import java.io.FileReader;
import java.util.List;

/**
 * Created by furan on 12/8/14.
 */
public class Main {
    public static void main(String[] args) {
/* Start the parser */
        try {
            parser p = new parser(new Lexer(new FileReader("src/test.txt")));
            List<StatementNode> result = (List<StatementNode>) p.parse().value;

            String code= CodeGenerator.GenerarCodigo(result, VariableGenerator.getInstance());

            System.out.println(code);




        } catch (Exception e) {
            /* do cleanup here -- possibly rethrow e */
            e.printStackTrace();
        }
    }
}
