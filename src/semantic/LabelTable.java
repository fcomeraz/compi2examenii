package semantic;

import types.Type;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by furan on 12/8/14.
 */
public class LabelTable {
    static LabelTable instance ;

    static{
        instance = new LabelTable();
    }

    public static  LabelTable getInstance()
    {
        return  instance;
    }

    private LabelTable()
    {

    }

    List<String> labels = new ArrayList<String>();

    public void declareLabel(String name) throws Exception {
        if(SymbolTable.getInstance().variableExists(name)) {
            throw new Exception("no se puede utilizar '" + name + "' debido a que ya se ha utilizado como nombre de variable");
        }

        if(!labelExists(name)){
            labels.add(name);
        }else {
            throw new Exception("ya se ha definido el label '" + name + "' anteriormente");
        }
    }

    public boolean labelExists(String name){
        return labels.contains(name);
    }
}
