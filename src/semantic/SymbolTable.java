package semantic;

import codegeneration.VariableGenerator;
import types.IntegerType;
import types.Type;

import java.util.Hashtable;

/**
 * Created by furan on 12/8/14.
 */
public class SymbolTable
{
    static SymbolTable instance ;

    static{
        instance = new SymbolTable();
    }

    public static  SymbolTable getInstance()
    {
        return  instance;
    }

    private SymbolTable()
    {

    }

    Hashtable<String, Type> variables = new Hashtable<String, Type>();

    public void declareVariable(String name, Type type) throws Exception {
        if(LabelTable.getInstance().labelExists(name)){
            throw new Exception("no se puede utilizar '" + name + "' debido a que ya se ha utilizado como label");
        }

        if(!variableExists(name)){
            variables.put(name,type);
            if(type instanceof IntegerType) {
                VariableGenerator.getInstance().declareIntVariable(name);
            }
        }
    }

    public boolean variableExists(String name){
        return variables.containsKey(name);
    }

}