package codegeneration;

import java.util.ArrayList;

/**
 * Created by furan on 12/8/14.
 */
public class VariableGenerator {
    static VariableGenerator instance ;

    static{
        instance = new VariableGenerator();
    }

    public static  VariableGenerator getInstance()
    {
        return  instance;
    }

    private VariableGenerator()
    {

    }


    ArrayList<VariableDeclaration> variables = new ArrayList<VariableDeclaration>();

    public void declareIntVariable(String name)
    {
        variables.add(new VariableDeclaration(name,"dd","0"));
    }
}
