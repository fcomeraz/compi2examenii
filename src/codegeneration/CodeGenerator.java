package codegeneration;

import tree.statement.StatementNode;

import java.util.List;

/**
 * Created by furan on 12/9/14.
 */
public class CodeGenerator {
    public static String GenerarCodigo(List<StatementNode> result, VariableGenerator vars) throws Exception {
        String code="format PE CONSOLE 4.0\n"+
                "entry main\n\n"+
                "include 'win32a.inc'\n\n"+
                "section '.data' data readable writeable    \n\n"+
                "     pausa db 'pause', 0\n" +
                "     printInt db '%d' ,10 , 0\n"+
                "     readInt db '%d', 0\n";


        for(VariableDeclaration var:vars.variables){
            code += "     @" + var.getName() + "@ " + var.getDeclarationType() + ' ' + var.getValue() + '\n';
        }



        code += "\nsection '.code' code readable executable\n\n" +
                "  main:    ";

        for(StatementNode stmnt:result)
        {
            code+= stmnt.generateCode();
        }

        return code+= "\n     ;system(\"pause\");\n" +
                "     push pausa\n" +
                "     call [system]\n" +
                "     add sp, 4\n\n" +
                "     ;return 0;\n" +
                "     call [ExitProcess]\n\n" +
                "section '.idata' import data readable writeable\n\n" +
                "  library kernel,'KERNEL32.DLL',\\\n" +
                "          ms ,'msvcrt.dll'\n\n" +
                "  import kernel,\\\n" +
                "         ExitProcess,'ExitProcess'\n\n" +
                "  import ms,\\\n" +
                "         printf,'printf',\\\n" +
                "         cget ,'_getch',\\\n" +
                "         system,'system', \\\n" +
                "         scanf, 'scanf'                   \n";
    }
}
