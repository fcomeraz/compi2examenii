package tree.expression;

import codegeneration.ExpressionCode;

/**
 * Created by furan on 12/8/14.
 */
public class LessOrEqualThanNode extends BinaryOperationNode {


    @Override
    public ExpressionCode GenerateCode() {
        ExpressionCode lValue = t1.GenerateCode();
        ExpressionCode rValue = t2.GenerateCode();

        String codigo = lValue.getCode() + rValue.getCode() +
                "     mov eax, " + lValue.getDestination() + "\n" +
                "     mov ebx, " + rValue.getDestination() + "\n" +
                "     cmp eax, ebx\n";

        return new ExpressionCode(codigo, "");
    }

    @Override
    public String toString() {
        return t1.toString() + " <= " + t2.toString();
    }
}
