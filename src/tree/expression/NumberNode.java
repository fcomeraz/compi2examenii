package tree.expression;

import codegeneration.ExpressionCode;

/**
 * Created by furan on 12/8/14.
 */
public class NumberNode extends ExpressionNode {
    private Integer value;

    public NumberNode(Integer value) {
        this.value = value;
    }


    @Override
    public ExpressionCode GenerateCode() {
        return new ExpressionCode("", String.valueOf(value));
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
