package tree.expression;

import codegeneration.ExpressionCode;
import types.Type;

/**
 * Created by furan on 12/8/14.
 */
public abstract class BinaryOperationNode extends ExpressionNode {
    ExpressionNode t1;
    ExpressionNode t2;

    public void SetLeft(ExpressionNode t1){
        this.t1 = t1;
    }

    public void SetRight(ExpressionNode t2){
        this.t2 = t2;
    }

    @Override
    public ExpressionCode GenerateCode() {
        return null;
    }


}
