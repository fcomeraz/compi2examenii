package tree.expression;

import codegeneration.ExpressionCode;

/**
 * Created by furan on 12/8/14.
 */
public class SumNode extends BinaryOperationNode {

    @Override
    public ExpressionCode GenerateCode() {
        ExpressionCode lVal = t1.GenerateCode();
        ExpressionCode rVal = t2.GenerateCode();

        String code = lVal.getCode() + rVal.getCode() +
                      "     mov eax, " + lVal.getDestination()+'\n'+
                      "     add eax, " + rVal.getDestination()+'\n';

        return new ExpressionCode(code, "eax");
    }

    @Override
    public String toString() {
        return t1.toString() + " + " + t2.toString();
    }
}
