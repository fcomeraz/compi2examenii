package tree.expression;

import codegeneration.ExpressionCode;
import types.Type;

/**
 * Created by furan on 12/8/14.
 */
public abstract class ExpressionNode {

    public abstract ExpressionCode GenerateCode();


}
