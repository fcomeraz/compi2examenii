package tree.expression;

import codegeneration.ExpressionCode;

/**
 * Created by furan on 12/8/14.
 */
public class IdNode extends ExpressionNode {
    private String name;

    public IdNode(String name) {
        this.name = name;
    }

    @Override
    public ExpressionCode GenerateCode() {
        return new ExpressionCode("", "[@"+name+"@]");
    }


    @Override
    public String toString() {
        return name;
    }
}
