package tree.expression;

import codegeneration.ExpressionCode;

/**
 * Created by furan on 12/8/14.
 */
public class DivNode extends BinaryOperationNode {


    @Override
    public ExpressionCode GenerateCode() {
        ExpressionCode lVal = t1.GenerateCode();
        ExpressionCode rVal = t2.GenerateCode();

        String code = lVal.getCode() + rVal.getCode() +
                "     mov edx, 0\n"+
                "     mov eax, " + lVal.getDestination()+'\n'+
                "     mov ecx, " + rVal.getDestination()+'\n'+
                "     div ecx\n";

        return new ExpressionCode(code, "eax");
    }

    @Override
    public String toString() {
        return t1.toString() + " / " + t2.toString();
    }
}
