package tree.statement;

/**
 * Created by furan on 12/8/14.
 */
public abstract class StatementNode {

    public abstract String generateCode() throws Exception;

}
