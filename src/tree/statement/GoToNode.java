package tree.statement;

import semantic.LabelTable;

/**
 * Created by furan on 12/8/14.
 */
public class GoToNode extends StatementNode {
    private LabelNode label;

    public GoToNode(LabelNode label)  {

        this.label = label;
    }

    @Override
    public String generateCode() throws Exception {
        if(!LabelTable.getInstance().labelExists(label.toString())){
            throw  new Exception("El label '" + label.toString() + "' no existe");
        }
        return "\n     " + toString() + '\n'+
                "     jmp @"+label+"@"+'\n';
    }

    @Override
    public String toString() {
        return ";goto " + label;
    }
}
