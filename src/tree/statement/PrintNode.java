package tree.statement;

import codegeneration.ExpressionCode;
import tree.expression.ExpressionNode;
import types.Type;

/**
 * Created by furan on 12/8/14.
 */
public class PrintNode extends StatementNode {


    private ExpressionNode exp;

    public PrintNode(ExpressionNode exp) {
        this.exp = exp;
    }

    @Override
    public String generateCode() {
        ExpressionCode expCode = exp.GenerateCode();

        return "\n" + expCode.getCode() +
               "     ;" + toString() + '\n' +
               "     push " + expCode.getDestination() + "\n"+
               "     push printInt\n     call [printf]\n     add sp, 8\n";

    }

    @Override
    public String toString() {
        return "print " + exp.toString();
    }
}
