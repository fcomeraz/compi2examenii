package tree.statement;

import codegeneration.ExpressionCode;
import tree.expression.*;

/**
 * Created by furan on 12/8/14.
 */
public class IfNode extends StatementNode {
    private LabelNode label;
    private BinaryOperationNode binaryOper;

    public IfNode(BinaryOperationNode binaryOper, LabelNode label) {
        this.binaryOper = binaryOper;
        this.label = label;
    }

    @Override
    public String generateCode() {
        ExpressionCode exp = binaryOper.GenerateCode();

        String code = "\n     ;" + toString() + "\n" +
                      exp.getCode();

        if(binaryOper instanceof EqualNode){
            code += "     je @" + label.toString()+"@\n";
        } else if (binaryOper instanceof NotEqualNode){
            code += "     jne @" + label.toString()+"@\n";
        } else if (binaryOper instanceof GreaterThanNode){
            code += "     jg @" + label.toString()+"@\n";
        } else if (binaryOper instanceof GreaterOrEqualThanNode){
            code += "     jge @" + label.toString()+"@\n";
        } else if (binaryOper instanceof LessThanNode){
            code += "     jl @" + label.toString()+"@\n";
        } else if (binaryOper instanceof LessOrEqualThanNode){
            code += "     jle @" + label.toString()+"@\n";
        }
        return code;
    }

    @Override
    public String toString() {
        return "if " + binaryOper.toString() + " goto " + label.toString();
    }
}
