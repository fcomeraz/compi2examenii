package tree.statement;

import codegeneration.ExpressionCode;
import tree.expression.ExpressionNode;
import tree.expression.IdNode;

/**
 * Created by furan on 12/8/14.
 */
public class AssignNode extends StatementNode {

    private final ExpressionNode exp;
    private final IdNode id;

    public AssignNode(IdNode id, ExpressionNode exp) {
        this.id = id;
        this.exp = exp;
    }

    @Override
    public String generateCode() {
        ExpressionCode lVal = id.GenerateCode();
        ExpressionCode rVal = exp.GenerateCode();

        if(!"eax".equals(rVal.getDestination())) {
            return "\n     ;" + id.toString() + " := " + exp.toString() + '\n' +
                    lVal.getCode() + rVal.getCode() +
                    "     mov eax," + rVal.getDestination() + "\n" +
                    "     mov " + lVal.getDestination() + ",eax\n";
        }else{
            return "\n     ;" + id.toString() + " := " + exp.toString() + '\n' +
                    lVal.getCode() + rVal.getCode() +
                    "     mov " + lVal.getDestination() + ",eax\n";
        }
    }
}
