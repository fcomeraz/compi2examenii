package tree.statement;

import semantic.LabelTable;

/**
 * Created by furan on 12/8/14.
 */
public class LabelNode extends StatementNode {
    private final String label;

    public LabelNode(String label) {
        this.label = label;
    }

    @Override
    public String generateCode() {

        return "\n  ;" + toString() + ":\n"+
                "  @" + label + "@" + ":\n";
    }

    @Override
    public String toString() {
        return label;
    }
}
