package tree.statement;

import tree.expression.IdNode;

/**
 * Created by furan on 12/8/14.
 */
public class ReadNode extends StatementNode {
    private IdNode id;

    public ReadNode(IdNode id) {
        this.id = id;
    }

    @Override
    public String generateCode() {
        return "\n     ;" + toString() + '\n'+
               "     push @" + id.toString() + "@\n"+
               "     push readInt\n"+
               "     call [scanf]\n";
    }

    @Override
    public String toString() {
        return "read " + id.toString();
    }
}
