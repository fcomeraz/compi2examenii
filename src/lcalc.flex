import java_cup.runtime.*;
%%
%class Lexer
%line
%column
%cup
%{
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }

    private Symbol symbol(int type, Object varue) {
        return new Symbol(type, yyline, yycolumn, varue);
    }
%}
LineTerminator = \r|\n|\r\n
WhiteSpace     = [ \t\f]
dec_int_lit = 0 | [1-9][0-9]*
dec_int_id = [A-Za-z_][A-Za-z_0-9]*
%%
<YYINITIAL> {
    ":"                { return symbol(sym.COLON);}
    ":="               { return symbol(sym.ASSIGN);}
    "+"                { return symbol(sym.PLUS);}
    "-"                { return symbol(sym.MINUS);}
    "*"                { return symbol(sym.MULT);}
    "/"                { return symbol(sym.DIVIDE);}
    "if"               { return symbol(sym.IF);}
    "goto"             { return symbol(sym.GOTO);}
    "print"            { return symbol(sym.PRINT);}
    "read"             { return symbol(sym.READ);}
    "=="               { return symbol(sym.EQUALS);}
    "!="               { return symbol(sym.NOTEQUALS);}
    ">"                { return symbol(sym.GREATERTHAN); }
    "<"                { return symbol(sym.LESSTHAN); }
    ">="               { return symbol(sym.GREATEROREQUALTHAN); }
    "<="               { return symbol(sym.LESSOREQUALTHAN);}
    {LineTerminator}   { return symbol(sym.EOL);}
    {dec_int_lit}      { return symbol(sym.NUMBER, new Integer(yytext())); }
    {dec_int_id}       { return symbol(sym.ID, yytext() );}
    {WhiteSpace}       { /* just skip what was found, do nothing */ }
}
[^]                    { throw new Error("Illegal character <"+yytext()+">"); }
